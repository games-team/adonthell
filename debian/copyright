Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Adonthell

Files: *
Copyright:
 1997-2001 by Dimitri van Heesch
 1999 The Adonthell Team
 1999-2001 The Adonthell Project
 1999-2016 Kai Sterker <kaisterker@linuxgames.com>
 1999-2004 Alexandre Courbot <alexandrecourbot@linuxgames.com>
 2000-2001, 2004 Joel Vennin
 2000-2001 Michael Smith <msmith@labyrinth.net.au>
 2000 Andrew Henderson <hendersa@db.erau.edu>
 1995-1998, 2000-2002, 2004 Free Software Foundation, Inc.
License: GPL-2+

Files:
 src/gettext.h
 config.guess
 config.sub
Copyright: 1995-1998, 2000-2002, 2004-2006, 2009-2011, 2015 Free Software Foundation, Inc
License: GPL-3+

Files: install-sh
Copyright: 1994 X Consortium
License: Expat
 # Permission is hereby granted, free of charge, to any person obtaining a copy
 # of this software and associated documentation files (the "Software"), to
 # deal in the Software without restriction, including without limitation the
 # rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 # sell copies of the Software, and to permit persons to whom the Software is
 # furnished to do so, subject to the following conditions:
 #
 # The above copyright notice and this permission notice shall be included in
 # all copies or substantial portions of the Software.
 #
 # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 # IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 # FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 # X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 # AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 # TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 #
 # Except as contained in this notice, the name of the X Consortium shall not
 # be used in advertising or otherwise to promote the sale, use or other deal-
 # ings in this Software without prior written authorization from the X Consor-
 # tium.
 #
 #
 # FSF changes to this file are in the public domain.

Files: debian/*
Copyright: 2002-2007, Gordon Fraser <gordon@debian.org>
           2006,      Mohammed Adnène Trojette <adn+deb@diwi.org>
           2008,      Clint Adams <schizo@debian.org>
           2008-2013, Barry deFreese <bddebian@comcast.net>
           2008,      Iain Lane <laney@ubuntu.com>
           2009-2010, Moritz Muehlenhoff <jmm@debian.org>
           2011,      Evgeni Golov <evgeni@debian.org>
           2016-2019, Markus Koschany <apo@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License 2 for more details.
 .
 The GNU GPL may be viewed on Debian systems in /usr/share/common-licenses/GPL-2.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License 3 for more details.
 .
 The GNU GPL may be viewed on Debian systems in /usr/share/common-licenses/GPL-3.
